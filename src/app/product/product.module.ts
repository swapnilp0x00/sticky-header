import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductPage } from './product.page';

import { BrandingComponent } from './components/branding/branding.component';
import { TablistComponent } from './components/tablist/tablist.component';
import { NutritionComponent } from './components/tabs/nutrition/nutrition.component';

const routes: Routes = [
  {
    path: '',
    component: ProductPage,
    children: [
      {
        path: ':id',
        children: [
          {
            path: '',
            redirectTo: 'nutrition',
            pathMatch: 'full'
          },
          {
            path: 'nutrition',
            component: NutritionComponent
          },
          {
            path: 'allergens',
            component: NutritionComponent
          }
        ]
      },
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [ProductPage, BrandingComponent, TablistComponent, NutritionComponent]
})
export class ProductPageModule {}
