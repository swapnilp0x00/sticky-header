import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
@Component({
  selector: 'app-tablist',
  templateUrl: './tablist.component.html',
  styleUrls: ['./tablist.component.scss']
})
export class TablistComponent implements OnInit, AfterViewInit {
  // @ViewChild('iontabbar') iontabbar;
  @ViewChild('root') root;
  @Input() showContent: boolean;
  tablist: Array<any> = [
    {
      'tab': 'nutrition',
      'icon': 'home',
      'label': 'Nutrition',
    },
    {
      'tab': 'ingredients',
      'icon': 'home',
      'label': 'Ingredients',
    },
    {
      'tab': 'allergens',
      'icon': 'home',
      'label': 'Allergens',
    },
    {
      'tab': 'other',
      'icon': 'home',
      'label': 'Other Informations',
    },
    {
      'tab': 'brand',
      'icon': 'home',
      'label': 'Brand',
    }
  ];
  slot = 'top';
  activeTab: number;
  fixed = false;
  constructor() { }
  ngOnInit() {
    this.activeTab = this.activeTab || 0;
  }
  ngAfterViewInit() {
    // setInterval(() => {
    //   console.log(this.m.el.offsetTop);
    // }, 1000);
  }
  setFixed(value: boolean) {
    this.fixed = value;
  }
  changeTab(i) {
    this.activeTab = i;
  }
}
