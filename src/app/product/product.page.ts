import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { IonContent } from '@ionic/angular';
import { BrandingComponent } from './components/branding/branding.component';
import { TablistComponent } from './components/tablist/tablist.component';
import { fromEvent } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit, AfterViewInit {
  hide = false;
  show = true;
  activeTab: number;
  
  id: string;
  dataUrl = 'assets/mocks/data.json';
  t1 = new Array(10);
  t2 = new Array(20);
  bool = true;
  bool2 = true;
  slot = "";
  scroll$: Observable<any>;
  @ViewChild('content') content;
  @ViewChild('tablist') tablist;
  @ViewChild('tablist2') tablist2: ElementRef;
  constructor(private route: ActivatedRoute, private http: HttpClient, private renderer: Renderer2, private elementRef: ElementRef) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    if (this.id) {
      this.loadData(this.id).subscribe((data) => {
        console.log(data);
      });
    }
    this.scroll$ = this.content.ionScroll.pipe(
      throttleTime(10),
    );
  }
  ngAfterViewInit() {
    // this.content.scrollEvents = true;
    // this.scroll$.subscribe((e) => {
    //   console.log(e);
    //   console.log(this.content);
    //   if (this.tablist) {
    //     console.log(this.tablist.root);
    //     console.log(e.detail.scrollTop);
    //     console.log(this.tablist.root.nativeElement.offsetTop);
    //     // Scroll Down
    //     const height = this.tablist.root.nativeElement.offsetHeight;
    //     if (e.detail.deltaY > 0 && (e.detail.scrollTop > (this.tablist.root.nativeElement.offsetTop))) {
    //       if (this.bool) {
    //         this.bool = false;
    //         // this.content.scrollByPoint(0, height, 100);
    //       }
    //     }
    //     // Scroll Up
    //     if (e.detail.deltaY < 0 && (e.detail.scrollTop < (this.tablist.root.nativeElement.offsetTop + height))) {
    //       if (!this.bool) {
    //         this.bool = true;
    //         // this.content.scrollByPoint(0, -height, 500);
    //       }
    //     }
    //   }
    // });
    console.log(this.elementRef);
    const nodeList = this.elementRef.nativeElement.querySelectorAll('.sticky');
    console.log(nodeList);
    const array = Array.from(nodeList);
    console.log(array);
    let top = 0;
    array.forEach((i) => {
      this.renderer.setStyle(i, 'top', top + 'px');
      top = top + 20;
      // TODO: Calculate height of item and add.
      // To fill those gaps
      // top = top + height;
      console.log(top);
    });
  }
  loadData(id: string) {
    return this.http.get(this.dataUrl);
  }
}
