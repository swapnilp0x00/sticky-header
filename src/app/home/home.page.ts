import { Component, ElementRef, OnInit, AfterViewInit, Renderer2, ViewChild } from '@angular/core';

interface Status {
  fixed: boolean;
  top: number;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {
  items: Array<Status>;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }
  ngOnInit() {
  }
  ngAfterViewInit() {
    console.log(this.elementRef);
    const nodeList = this.elementRef.nativeElement.querySelectorAll('.sticky');
    console.log(nodeList);
    const array = Array.from(nodeList);
    console.log(array);
    let top = 0;
    array.forEach((i) => {
      this.renderer.setStyle(i, 'top', top + 'px');
      top = top + 40;
      console.log(top);
    });
  }
}
